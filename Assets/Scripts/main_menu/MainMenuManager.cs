﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {
	
	void Start () {
//		GameObject uppercaseButton = GameObject.Find("uppercase_button");		
//		GameObject lowercaseButton = GameObject.Find("lowercase_button");
//		GameObject[] buttons = new[] {uppercaseButton, lowercaseButton};
		StartMusicLoop();
	}

	private void StartMusicLoop() {
		GameObject musicLoop = GameObject.Find("Music Loop");
		Debug.Log("Music loop" + musicLoop.name);
	}

	public void gotoLetterSelectionScene(string letterCaseString) {
		LetterCase letterCase = (LetterCase) Enum.Parse(typeof(LetterCase), letterCaseString);
		CrossSceneParameters.letterCase = letterCase;
		
		Debug.Log("Letter case: " + letterCase);
		
		SceneManager.LoadScene(Scene.LETTER_SELECTION);
	}
	
}
