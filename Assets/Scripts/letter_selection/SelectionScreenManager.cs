﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectionScreenManager : MonoBehaviour {

	private GameObject letterIcons;
	private GameObject letterCaseButton;
	private LetterCase letterCase;
	
	void Start () {
		this.letterCase = CrossSceneParameters.letterCase;
		
		this.letterIcons = GameObject.Find("letter_icons");
		this.letterIcons.GetComponent<LetterIcons>().updateLetterCase(this.letterCase);
		
		this.AddClickListenersToIcons();
		this.UpdateLetterCaseButton();
		this.UpdateLetterIconsLetterCase();
	}

	private void AddClickListenersToIcons() {
		foreach (Transform transform in letterIcons.transform) {
			if (transform.CompareTag("letter_icon")) {
				GameObject letterIcon = transform.gameObject;
				letterIcon.GetComponent<Button>().onClick.AddListener(() => { onLetterIconClick(letterIcon); });
			}
		}
	}

	private void onLetterIconClick(GameObject letterIcon) {
		string iconLetter = letterIcon.GetComponent<LetterIcon>().letter;
		LetterCase iconletterCase = letterIcon.GetComponent<LetterIcon>().letterCase;
		
		Debug.Log("Click " + iconLetter + " " + iconletterCase);

		CrossSceneParameters.letterCase = iconletterCase;
		CrossSceneParameters.letter = iconLetter;
//		SceneManager.LoadScene(Scene.GAMEPLAY);
	}

	private void UpdateLetterCaseButton() {
		int buttonState = (this.letterCase == LetterCase.UPPER) ? 1 : 2;
		
		this.letterCaseButton = GameObject.Find("lettercase_button");
		this.letterCaseButton.GetComponent<ToggleButton>().setState(buttonState);
	}

	private void UpdateLetterIconsLetterCase() {
		this.letterIcons.GetComponent<LetterIcons>().updateLetterCase(this.letterCase);
	}

	public void toggleLetterCase() {
		this.letterCase = (this.letterCase == LetterCase.UPPER)
			? LetterCase.LOWER
			: LetterCase.UPPER;
		
		this.UpdateLetterCaseButton();
		this.UpdateLetterIconsLetterCase();
	}

	public void gotoMainMenu() {
		Debug.Log("Goto Main Menu");
		
		SceneManager.LoadScene(Scene.MAIN_MENU);
	}
	
	
	
}
