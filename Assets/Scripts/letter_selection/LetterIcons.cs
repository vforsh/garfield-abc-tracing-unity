﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LetterIcons : MonoBehaviour {

	private List<GameObject> letterIcons;
	private const string alphabet = "abcdefghijklmnopqrstuvwxyz";

	void Start () {
		InitLetterIcons();
		SetLetters();
	}

	private void InitLetterIcons() {
		this.letterIcons = GameObject.FindGameObjectsWithTag("letter_icon").ToList();
	}

	private void SetLetters() {
		foreach (GameObject letterIcon in this.letterIcons) {
			int index = letterIcon.transform.GetSiblingIndex();
			if (index < alphabet.Length) {
				string letter = alphabet[index].ToString();
				letterIcon.GetComponent<LetterIcon>().letter = letter;
			}
		}
	}

	public void updateLetterCase(string letterCaseString) {
		LetterCase letterCase = (LetterCase) Enum.Parse(typeof(LetterCase), letterCaseString);
		
		this.updateLetterCase(letterCase);
	}
	
	public void updateLetterCase(LetterCase letterCase) {
		foreach (GameObject letterIcon in this.letterIcons) {
			letterIcon.GetComponent<LetterIcon>().letterCase = letterCase;
		}
	}
	
}
