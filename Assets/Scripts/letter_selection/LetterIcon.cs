﻿using UnityEngine;
using UnityEngine.UI;

public class LetterIcon : MonoBehaviour {

	private string _letter;
	public string letter {
		get { return _letter; }
		set {
			_letter = value;
			this.updateLetterText();
		}
	}

	private LetterCase _letterCase;
	public LetterCase letterCase {
		get { return _letterCase; }
		set {
			_letterCase = value;
			this.updateLetterText();
		}
	}
	
	void Start () {
		
	}

	private void OnMouseDown() {
		Debug.Log("Clicked! " + this._letter);
	}

	private void updateLetterText() {
		string textContent = (_letterCase == LetterCase.UPPER) 
			? _letter.ToUpper() 
			: _letter.ToLower();
		
		this.transform.Find("letter").GetComponent<Text>().text = textContent;
	}
	
}
