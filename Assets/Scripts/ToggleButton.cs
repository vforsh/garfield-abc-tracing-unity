﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleButton : MonoBehaviour {

    public Sprite state_1;
    public Sprite state_2;
    public int currentState = 1;

    private void Start() {
        gameObject.GetComponent<Button>().onClick.AddListener(toggleState);
    }

    public void toggleState() {
        this.currentState = (this.currentState == 1) ? 2 : 1;
        this.updateSprite();
    }

    public void setState(int state) {
        this.currentState = state;
        this.updateSprite();
    }

    private void updateSprite() {
        Sprite newSprite = (this.currentState == 1)
            ? this.state_1
            : this.state_2;

        gameObject.GetComponent<Image>().sprite = newSprite;   
    }
    
}