﻿public static class Scene {

    public const string PRELOADER = "preloader";
    public const string MAIN_MENU = "main_menu";
    public const string LETTER_SELECTION = "letter_selection";
    public const string COLLECTION = "collection";
    public const string GAMEPLAY = "gameplay";

}