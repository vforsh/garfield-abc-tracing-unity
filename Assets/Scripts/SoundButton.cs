﻿using UnityEngine;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour {
    
    private void Awake() {
        int buttonState = AudioListener.pause ? 2 : 1;
        
        gameObject.GetComponent<ToggleButton>().setState(buttonState);
        gameObject.GetComponent<Button>().onClick.AddListener(onClick);    
    }

    private void onClick() {
        AudioListener.pause = !AudioListener.pause;
    }
    
}